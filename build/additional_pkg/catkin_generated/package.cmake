set(_CATKIN_CURRENT_PACKAGE "additional_pkg")
set(additional_pkg_VERSION "0.0.0")
set(additional_pkg_MAINTAINER "kevin <kevin@todo.todo>")
set(additional_pkg_PACKAGE_FORMAT "2")
set(additional_pkg_BUILD_DEPENDS "roscpp" "rospy" "std_msgs")
set(additional_pkg_BUILD_EXPORT_DEPENDS "roscpp" "rospy" "std_msgs")
set(additional_pkg_BUILDTOOL_DEPENDS "catkin")
set(additional_pkg_BUILDTOOL_EXPORT_DEPENDS )
set(additional_pkg_EXEC_DEPENDS "roscpp" "rospy" "std_msgs")
set(additional_pkg_RUN_DEPENDS "roscpp" "rospy" "std_msgs")
set(additional_pkg_TEST_DEPENDS )
set(additional_pkg_DOC_DEPENDS )
set(additional_pkg_URL_WEBSITE "")
set(additional_pkg_URL_BUGTRACKER "")
set(additional_pkg_URL_REPOSITORY "")
set(additional_pkg_DEPRECATED "")