# CMake generated Testfile for 
# Source directory: /home/kevin/Documents/jackal_ws/src
# Build directory: /home/kevin/Documents/jackal_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("jackal_desktop/jackal_desktop")
subdirs("jackal_simulator/jackal_simulator")
subdirs("jackal/jackal_tutorials")
subdirs("jackal/jackal_msgs")
subdirs("additional_pkg")
subdirs("jackal/jackal_control")
subdirs("jackal/jackal_description")
subdirs("jackal_simulator/jackal_gazebo")
subdirs("jackal/jackal_navigation")
subdirs("jackal_desktop/jackal_viz")
subdirs("interactive_marker_twist_server")
